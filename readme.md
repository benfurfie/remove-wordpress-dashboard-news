# Remove WordPress Dashboard News

Out of the box, WordPress displays news about WordPress and events near the user on the dashboard. These are almost always developer focused and irrelevant to the client.

This plugin completely removes it from the dashboard.