<?php
/**
 * @package remove-wordpress-wordpress-dashboard-news
 * @version 0.1.0
 */

/*
  Plugin Name: Remove WordPress Dashboard News
  Author: Ben Furfie
  Author URI: http://www.benfurfie.co.uk
  Version: 0.1.0
  Copyright: © 2017 Ben Furfie
  License: proprietary
*/

defined('ABSPATH') or die('You cannot access this page directly.');

/**
 * Remove the WordPress News metabox from the dashboard
 * 
 * @since 0.1.0
 */

function remove_wordpress_news_metabox() {
    remove_meta_box('dashboard_primary', 'dashboard', 'normal'); // Removes the 'WordPress News' widget
}
add_action('admin_init', 'remove_wordpress_news_metabox');